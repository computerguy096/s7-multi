# S7 Multi

This is a simple PCB adapter, which allows you to set all the multipliers of K6-2/Tillamook chips using older Socket 7 boards:

The PGA-321 footprint was kindly provided by [Rigo](https://github.com/rigred/kicad-retro/tree/main/processors/socket%207) , make sure to checkout his github page for other cool stuff.